#pragma once

#include <stdexcept>

/// custom exception for shutting down the program when receiving a 'shutdown' message
class ShutdownException : public std::logic_error
{
public:
    /// constructor setting message parameter
    /// \param message string message to attach to error
    ShutdownException(const std::string & message) :
            std::logic_error(message)
    {}
};