#include "ResponseMessage.h"

ResponseMessage::ResponseMessage() : Message()
{
}

ResponseMessage::ResponseMessage(const Command command, const std::string &identifier, const std::string &parameter)
        : Message(command, identifier, parameter)
{
}

std::string ResponseMessage::toString() const
{
    // <command>:[<identifier>;]<parameter>\n
    std::string identifierString = identifier.empty() ? "" : identifier + ";";
    return commandToString(command) + ":" + identifierString + parameter + "\n";
}
