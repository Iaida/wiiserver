#include "DeviceManager.h"
#include "Server.h"
#include "util/Timer.h"


int main()
{
    std::cout << "Starting... \n";

    DeviceManager deviceManager;
    boost::asio::io_service io_service;

    // start separate server thread with reference to the deviceManager object
    std::thread serverThread = std::thread([&]()
                               {
                                   try
                                   {
                                       server s(io_service, deviceManager);
                                       io_service.run();
                                   }
                                   catch (std::exception& e)
                                   {
                                       std::cerr << "Exception: " << e.what() << "\n";
                                   }
                               });

    std::cout << "Ready to receive connections \n";

    // execute only 100 times per second -> polling rate of wii devices
    Timer timer(Timer::hz100);
    while (true)
    {
        if (timer.check())
        {
            try
            {
                deviceManager.update();
            }
            catch (const ShutdownException & e)
            {
                io_service.stop();
                if (serverThread.joinable())
                    serverThread.join();
                return EXIT_SUCCESS;
            }
        }

        timer.sleepRestDuration();
    }
}

