#pragma once


#include "Message.h"

class ResponseMessage : public Message
{
public:
    /// same default constructor as Message
    ResponseMessage();
    /// same constructor setting members as Message
    ResponseMessage(const Command command, const std::string & identifier, const std::string & parameter);

    /// transforms a response message to a formatted string
    /// \return string in the format: '<command>:[<identifier>;]<parameter>\n'
    std::string toString() const;
};
