#pragma once

#include "enum/DeviceType.h"
#include "enum/Command.h"
#include <wiicpp.h>


/// base class representing a wii device which handles events, requests and
/// uses cwii library-functionality to access the current state
class Device
{
public:
    /// default constructor initializing cWiimotePtr with null
    Device();
    /// virtual destructor
    virtual ~Device() = default;

    /// checks if the device object is synchronized with the cwii-library's object
    /// \return returns true if synchronized, else false
    bool synchronized() const;
    /// checks if synchronized and accesses the device's battery level
    /// \return returns a floating point value between 0 and 1 representing the percentage of power remaining
    float getBatteryLevel() const;

    /// virtual function used to handle device-unspecific events,
    /// called in derived classes before their own implementation
    virtual void processEvents();
    /// virtual function used to desynchronize from the cwii-library's object, used before reconnecting
    virtual void desync();
    /// virtual setter, which synchronizes the device object with the cwii-library's object
    /// \param newCWiimotePtr the CWiimote pointer to be used
    virtual void setCWiimotePtr(CWiimote *newCWiimotePtr);

    /// pure virtual function implemented by derived classes for handling requests
    /// \param command command enum value of the request
    /// \param identifierString MAC address or alias of the device which should handle the request
    /// \param parameterString parameter of the command
    /// \return response string
    virtual std::string handleRequest(const Command command, const std::string & identifierString, const std::string & parameterString) = 0;
    /// pure virtual function implemented by derived classes for sending messages when streaming the device state
    /// \param identifier MAC address or alias of the device which should handle the request
    /// \return response string containing the device state
    virtual std::string streamMessage(const std::string &identifier) = 0;
    /// pure virtual function implemented by derived classes for getting their device type
    /// \return the device type enum for the derived wii device
    virtual DeviceType getDeviceType() const = 0;

protected:
    /// pointer to the matching cwii-library object
    CWiimote * cWiimotePtr;
};