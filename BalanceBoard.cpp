#include "BalanceBoard.h"

#include "ErrorCodeException.h"
#include "ResponseMessage.h"

BalanceBoard::BalanceBoard() :
        balanceBoardPtr(nullptr)
{}

void BalanceBoard::processEvents()
{
    Device::processEvents();

    if (synchronized())
    {
        switch(cWiimotePtr->GetEvent())
        {
            // check if delayed balance board expansion detected
            case CWiimote::EVENT_BALANCE_BOARD_INSERTED:
                std::cout << "EVENT_BALANCE_BOARD_INSERTED \n";
                balanceBoardPtr = &(cWiimotePtr->ExpansionDevice.BalanceBoard);
                break;
            case CWiimote::EVENT_BALANCE_BOARD_REMOVED:
                std::cout << "EVENT_BALANCE_BOARD_REMOVED \n";
                balanceBoardPtr = nullptr;
            default:
                break;
        }
    }

}

std::string BalanceBoard::handleRequest(const Command command,
                                        const std::string &identifierString,
                                        const std::string &parameterString)
{
    switch (command)
    {
        case TYPE:
            // TYPE <identifier>
            if (parameterString.empty())
            {
                // TYPE:<identifier>;<devicetype>
                return ResponseMessage(command, identifierString, deviceTypeToString(getDeviceType())).toString();
            }
            else
                throw NotAValidParameter();

        case GET:
            // GET <identifier>
            if (parameterString.empty())
            {
                // GET:<identifier>;<top-left>,<top-right>,<bottom-left>,<bottom-right>,<total>\n
                return ResponseMessage(command, identifierString, weightString()).toString();
            }
            else
                throw NotAValidParameter();
        case BATTERY:
            // BATTERY <identifier>
            if (parameterString.empty())
            {
                // BATTERY:<identifier>;<value>
                return ResponseMessage(command, identifierString, std::to_string(getBatteryLevel())).toString();
            }
            else
                throw NotAValidParameter();
        default:
            throw CommandAndDeviceNotCompatible();
    }
}

bool BalanceBoard::isBalanceBoardCommand(const Command command)
{
    return command & balanceBoardCommand;
}

bool BalanceBoard::balanceBoardDetected() const
{
    return balanceBoardPtr != nullptr;
}

std::string BalanceBoard::streamMessage(const std::string &identifier)
{
    // STREAM:<identifier>;<top-left>,<top-right>,<bottom-left>,<bottom-right>,<total>\n
    return ResponseMessage(STREAM, identifier, weightString()).toString();
}


DeviceType BalanceBoard::getDeviceType() const
{
    return DeviceType::BALANCEBOARD;
}

std::string BalanceBoard::weightString()
{
    // default values in case balance board expansion not yet detected
    float topLeft = 0.0f;
    float topRight = 0.0f;
    float bottomLeft = 0.0f;
    float bottomRight = 0.0f;
    float total = 0.0f;

    if (balanceBoardDetected())
        balanceBoardPtr->WeightSensor.GetWeight(total, topLeft, topRight, bottomLeft, bottomRight);
    // <top-left>,<top-right>,<bottom-left>,<bottom-right>,<total>
    return std::to_string(topLeft) + "," +
           std::to_string(topRight) + "," +
           std::to_string(bottomLeft) + "," +
           std::to_string(bottomRight) + "," +
           std::to_string(total);
}

void BalanceBoard::desync()
{
    Device::desync();
    balanceBoardPtr = nullptr;
}


