#pragma once

#include "Device.h"

/// class representing a balanceboard which handles events, requests and
/// uses cwii library-functionality to access the current balanceboard state
class BalanceBoard : public Device
{
public:
    BalanceBoard();

    /// function used to handle balanceboard-specific events
    void processEvents() override;
    /// function for handling balanceboard-specific requests
    /// \param command command enum value of the request
    /// \param identifierString MAC address or alias of the device which should handle the request
    /// \param parameterString parameter of the command
    /// \return response string
    std::string handleRequest(const Command command, const std::string & identifierString, const std::string & parameterString) override;
    /// static function to check if a command can be handled by the BalanceBoard class
    /// \param command command to be checked
    /// \return true if the command can be handled, else false
    static bool isBalanceBoardCommand(const Command command);
    /// function for sending messages when streaming the balanceboard state
    /// \param identifier MAC address or alias of the device which should handle the request
    /// \return response string containing the device state
    std::string streamMessage(const std::string &identifier) override;
    DeviceType getDeviceType() const override;
    /// function used to desynchronize from the cwii-library's object, used before reconnecting,
    /// calls base class implementation and sets balanceBoardPtr to null
    void desync() override;

private:
    /// pointer to the matching cwii-library balance board
    CBalanceBoard * balanceBoardPtr;
    /// bitwise combined command for isBalanceBoardCommand
    static const unsigned balanceBoardCommand = TYPE | BATTERY | GET;

    /// checks if balanceboard expansion detected
    /// \return true if balanceboard expansion detected, else false
    bool balanceBoardDetected() const;
    /// returns weight string used in responses with values set to zero if no balance board detected
    /// \return string in the form '<top-left>,<top-right>,<bottom-left>,<bottom-right>,<total>'
    std::string weightString();
};