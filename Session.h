#pragma once

#include <boost/asio.hpp>
#include <mutex>
#include <thread>

#include "DeviceManager.h"

/// class representing a client session,
/// managing the communication with reponses and requests
/// as well as streaming
class session : public std::enable_shared_from_this<session>
{
public:
    /// constructor setting members
    /// \param socketToMove socket which has to be set with std::move
    /// \param deviceManager reference to the device manager to communicate with main thread
    session(boost::asio::ip::tcp::socket socketToMove, DeviceManager &deviceManager);
    /// destructor responsible for proper destruction of streamThread
    ~session();
    /// function called after initialization by the Server class, which starts reading incoming messages
    void start();
    /// static function to check if a command can be handled by the Session class
    /// \param command command to be checked
    /// \return true if the command can be handled, else false
    static bool isSessionCommand(const Command command);

private:
    /// function calling async_read_some and handling incoming messages
    void do_read();
    /// function calling async_write and sending responses
    /// \param message message to be sent
    void do_write(const std::string & message);
    /// function used to send stream-responses while streaming
    /// \param message stream response
    void do_stream_write(const std::string message);

    /// uses streamingMutex to read streaming member
    bool readStreaming();
    /// uses streamingMutex to set streaming member
    /// \param streamRunning new streaming state
    void setStreaming(bool streamRunning);
    /// function initializing streamThread
    void startStreaming();
    /// function setting streaming member to false
    void stopStreaming();

    /// function for handling session-specific requests
    /// \param command command enum value of the request
    /// \param identifierString MAC address or alias of the device which should handle the request
    /// \param parameterString parameter of the command
    /// \return response string
    std::string handleSessionRequest(const Command command, const std::string &identifier, const std::string &parameter);

    /// small function returning a prefix including the client's ip
    /// to differentiate between clients when reading the console output
    /// \return string containing ipString
    std::string debugPrefix() const;

    std::string getTimeStamp() const;

    /// bitwise combined command for isSessionCommand
    static const unsigned sessionCommand = STREAM | QUIT;

    /// boost socket, trailing underscore because of naming collision with linux socket.h header
    boost::asio::ip::tcp::socket socket_;
    /// charBuffer's maximum length
    static const size_t maxLength = 1024;
    /// character buffer for reading incoming messages
    std::array<char, maxLength> charBuffer;
    /// IP-address of the client
    std::string ipString;
    /// reference to the device manager to communicate with main thread
    DeviceManager & deviceManager;

    /// current streaming state
    bool streaming;
    /// mutex fdr accessing streaming member
    std::mutex streamingMutex;
    /// vector of identifier and device pairs which are currently streaming to the client
    std::vector<std::pair<std::string, Device*>> identifierDeviceStreams;
    /// thread running while streaming and sending messages
    std::thread streamThread;
};