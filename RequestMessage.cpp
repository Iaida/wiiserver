#include "RequestMessage.h"

#include "util/StringUtils.h"
#include "BalanceBoard.h"
#include "WiiMote.h"
#include "DeviceManager.h"
#include "Session.h"


RequestMessage::RequestMessage(const Command command, const std::string &identifier, const std::string &parameter) :
        Message(command, identifier, parameter)
{
}


RequestMessage::RequestMessage(const std::string &requestString)
{
    // split by space
    std::vector<std::string> splitResult = StringUtils::split(requestString, ' ');

    // assign Message members
    if (!splitResult.empty())
    {
        command = stringToCommand(splitResult[0]);

        if (session::isSessionCommand(command))
        {
            // examples:
            // STREAM wm
            // STREAM wm stop
            // QUIT

            if (splitResult.size() >= 2)
            {
                identifier = splitResult[1];

                if (splitResult.size() == 3)
                    parameter = splitResult[2];
            }
        }

        else if (DeviceManager::isGeneralCommand(command))
        {
            // command which doesn't need an identifier
            identifier = "";
            // initialize parameter to empty string if not present
            parameter = (splitResult.size() >= 2) ? splitResult[1] : "";
        }
        else if (BalanceBoard::isBalanceBoardCommand(command) ||
                 WiiMote::isWiiMoteCommand(command))
        {
            // command which needs an identifier

            // check if identifier string present
            if (splitResult.size() >= 2)
            {
                identifier = splitResult[1];

                if (splitResult.size() == 3)
                    parameter = splitResult[2];
                else if (splitResult.size() == 4)
                {
                    // only for led command with 2 separated parameters
                    parameter = splitResult[2] + " " + splitResult[3];
                }
                else
                    parameter = "";
            }
            else
                throw DeviceNotFound();
        }
        else
            throw NotAValidCommand();
    }
    else
        throw NotAValidCommand();
}


RequestMessage::RequestMessage() : Message()
{
}

