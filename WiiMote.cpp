#include "WiiMote.h"
#include "ErrorCodeException.h"
#include "enum/Button.h"
#include "enum/LED.h"
#include "util/StringUtils.h"
#include "ResponseMessage.h"


WiiMote::WiiMote() = default;

void WiiMote::processEvents()
{
    Device::processEvents();
}

std::string
WiiMote::handleRequest(const Command command, const std::string &identifierString, const std::string &parameterString)
{
    switch (command)
    {
        case TYPE:
            // TYPE <identifier>
            if (parameterString.empty())
            {
                // TYPE:<identifier>;<devicetype>
                return ResponseMessage(command, identifierString, deviceTypeToString(getDeviceType())).toString();
            }
            else
                throw NotAValidParameter();
        case BATTERY:
            // BATTERY <identifier>
            if (parameterString.empty())
            {
                // BATTERY:<identifier>;<value>
                return ResponseMessage(command, identifierString, std::to_string(getBatteryLevel())).toString();
            }
            else
                throw NotAValidParameter();
        case GET:
        {
            // GET <identifier> <btn>|<led>
            try
            {
                // can throw NotAValidParameter exception
                CButtons::ButtonDefs button = stringToButton(parameterString);
                bool down = cWiimotePtr->Buttons.isPressed(button) != 0;
                // GET:<identifier>;<state>
                return ResponseMessage(command, identifierString, (down ? "down" : "up")).toString();
            }
            catch (const NotAValidParameter & e)
            {
                // string was not a valid parameter for button, try led

                // can throw NotAValidParameter exception
                // same exception as for previous stringToButton, will be propagated if thrown
                LED led = stringToLED(parameterString);
                bool on = getLEDState(led);
                // GET:<identifier>;<state>
                return ResponseMessage(command, identifierString, (on ? "on" : "off")).toString();
            }
        }
        case ROT:
            // ROT <identifier>
            if (parameterString.empty())
            {
                // http://wiic.sourceforge.net/index.php?n=Doc.Accelerometers :
                // Without IR or MotionPlus, it is impossible to retrieve the yaw angle,
                // since the Z component reported by the accelerometer is parallel to the g gravity vector
                float pitch = 0.0f;
                float roll = 0.0f;
                float yaw = 0.0f;

                if (cWiimotePtr->isUsingACC())
                    cWiimotePtr->Accelerometer.GetOrientation(pitch, roll, yaw);

                // ROT:<identifier>;<roll>,<pitch>,<yaw>
                std::string rotString = std::to_string(roll) + "," + std::to_string(pitch) + "," + std::to_string(yaw);
                return ResponseMessage(command, identifierString, rotString).toString();
            }
            else
                throw NotAValidParameter();

        case RUMBLE:
            // RUMBLE <identifier> on|off
            if (parameterString == "on")
                cWiimotePtr->SetRumbleMode(CWiimote::ON);
            else if (parameterString == "off")
                cWiimotePtr->SetRumbleMode(CWiimote::OFF);
            else
                throw NotAValidParameter();
            return "";

        case SET:
        {
            // SET <identifier> <led> on|off

            std::vector<std::string> splitResult = StringUtils::split(parameterString, ' ');

            if (splitResult.size() == 2)
            {
                bool on;
                if (splitResult[1] == "on")
                    on = true;
                else if (splitResult[1] == "off")
                    on = false;
                else
                    throw NotAValidParameter();

                LED led = stringToLED(splitResult[0]);

                setLEDEnum(led, on);

                // does not require response if no errors occurred
                return "";
            }
            else
                throw NotAValidParameter();
        }

        // done with command switch
        default:
            throw CommandAndDeviceNotCompatible();
    }
}

bool WiiMote::isWiiMoteCommand(const Command command)
{

    return command & wiiMoteCommand;
}

std::string WiiMote::streamMessage(const std::string &identifier)
{
    // STREAM:<identifier>;ROT:<roll>,<pitch>,<yaw>;(BTN:<btn>,<zustand>[;|])*
    std::string streamString;
    streamString +=  "ROT:" + getOrientationString();

    bool down = false;
    for (auto & stringButtonPair : stringButtonMap)
    {
        if (synchronized())
            down = cWiimotePtr->Buttons.isPressed(stringButtonPair.second) != 0;
        streamString += ";BTN:" + stringButtonPair.first + "," + (down ? "down" : "up");
    }
    return ResponseMessage(Command::STREAM, identifier, streamString).toString();
}

void WiiMote::setCWiimotePtr(CWiimote *cWiimotePtr)
{
    Device::setCWiimotePtr(cWiimotePtr);

    // turn on measurement of rotation/orientation
    cWiimotePtr->SetMotionSensingMode(CWiimote::ON);
}

CWiimote::LEDS WiiMote::ledIndexToBitmask(const int index) const
{
    if (cWiimotePtr->isLEDSet(index) != 0)
    {
        switch (index)
        {
            case 1:
                return CWiimote::LED_1;
            case 2:
                return CWiimote::LED_2;
            case 3:
                return CWiimote::LED_3;
            case 4:
                return CWiimote::LED_4;
            default:
                return CWiimote::LED_NONE;
        }
    }
    else
        return CWiimote::LED_NONE;
}

void WiiMote::setLEDEnum(const LED led, const bool on)
{
    switch (led)
    {
        case LED_ALL:
            cWiimotePtr->SetLEDs(on ? CWiimote::LED_1 | CWiimote::LED_2 | CWiimote::LED_3 | CWiimote::LED_4 : CWiimote::LED_NONE);
            break;
        case LED_1:
            cWiimotePtr->SetLEDs((on ? CWiimote::LED_1 : CWiimote::LED_NONE) |
                              ledIndexToBitmask(2) |
                              ledIndexToBitmask(3) |
                              ledIndexToBitmask(4));
            break;
        case LED_2:
            cWiimotePtr->SetLEDs(ledIndexToBitmask(1) |
                              (on ? CWiimote::LED_2 : CWiimote::LED_NONE) |
                              ledIndexToBitmask(3) |
                              ledIndexToBitmask(4));
            break;
        case LED_3:
            cWiimotePtr->SetLEDs(ledIndexToBitmask(1) |
                              ledIndexToBitmask(2) |
                              (on ? CWiimote::LED_3 : CWiimote::LED_NONE) |
                              ledIndexToBitmask(4));
            break;
        case LED_4:
            cWiimotePtr->SetLEDs(ledIndexToBitmask(1) |
                              ledIndexToBitmask(2) |
                              ledIndexToBitmask(3) |
                              (on ? CWiimote::LED_4 : CWiimote::LED_NONE));
            break;
    }
}

DeviceType WiiMote::getDeviceType() const
{
    return DeviceType::WIIMOTE;
}

std::string WiiMote::getOrientationString()
{
    float pitch = 0.0f;
    float roll = 0.0f;
    float yaw = 0.0f;

    if (synchronized() && cWiimotePtr->isUsingACC())
        cWiimotePtr->Accelerometer.GetOrientation(pitch, roll, yaw);

    return std::to_string(roll) + "," + std::to_string(pitch) + "," + std::to_string(yaw);
}

bool WiiMote::getLEDState(const LED led)
{
    bool on;
    if (led == LED_ALL)
        on = cWiimotePtr->isLEDSet(1) && cWiimotePtr->isLEDSet(2) && cWiimotePtr->isLEDSet(3) && cWiimotePtr->isLEDSet(4);
    else
    {
        int intFromLED =  static_cast<int>(led);
        on = (cWiimotePtr->isLEDSet(intFromLED) != 0);
    }


    return on;
}

