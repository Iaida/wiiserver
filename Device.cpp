#include "Device.h"

Device::Device() :
        cWiimotePtr(nullptr)
{
}

void Device::processEvents()
{
    if (synchronized())
    {
        switch(cWiimotePtr->GetEvent())
        {
            case CWiimote::EVENT_STATUS:
                std::cout << "EVENT_STATUS \n";
                break;
            case CWiimote::EVENT_CONNECT:
                std::cout << "EVENT_CONNECT \n";
                break;
            case CWiimote::EVENT_DISCONNECT:
                std::cout << "EVENT_DISCONNECT \n";
                break;
            case CWiimote::EVENT_UNEXPECTED_DISCONNECT:
                std::cout << "EVENT_UNEXPECTED_DISCONNECT \n";
                break;
            case CWiimote::EVENT_READ_DATA:
                std::cout << "EVENT_READ_DATA \n";
                break;
            case CWiimote::EVENT_NUNCHUK_INSERTED:
                std::cout << "EVENT_NUNCHUK_INSERTED \n";
                break;
            case CWiimote::EVENT_NUNCHUK_REMOVED:
                std::cout << "EVENT_NUNCHUK_REMOVED \n";
                break;
            case CWiimote::EVENT_CLASSIC_CTRL_INSERTED:
                std::cout << "EVENT_CLASSIC_CTRL_INSERTED \n";
                break;
            case CWiimote::EVENT_CLASSIC_CTRL_REMOVED:
                std::cout << "EVENT_CLASSIC_CTRL_REMOVED \n";
                break;
            case CWiimote::EVENT_GUITAR_HERO_3_CTRL_INSERTED:
                std::cout << "EVENT_GUITAR_HERO_3_CTRL_INSERTED \n";
                break;
            case CWiimote::EVENT_GUITAR_HERO_3_CTRL_REMOVED:
                std::cout << "EVENT_GUITAR_HERO_3_CTRL_REMOVED \n";
                break;
            case CWiimote::EVENT_MOTION_PLUS_INSERTED:
                std::cout << "EVENT_MOTION_PLUS_INSERTED \n";
                break;
            case CWiimote::EVENT_MOTION_PLUS_REMOVED:
                std::cout << "EVENT_MOTION_PLUS_REMOVED \n";
                break;
            default:
                break;
        }
    }

}

bool Device::synchronized() const
{
    return cWiimotePtr != nullptr;
}


void Device::setCWiimotePtr(CWiimote *newCWiimotePtr)
{
    cWiimotePtr = newCWiimotePtr;
}

float Device::getBatteryLevel() const
{
    return synchronized() ? cWiimotePtr->GetBatteryLevel() : 0.0f;
}

void Device::desync()
{
    cWiimotePtr = nullptr;
}
