#pragma once

#include "Device.h"
#include "RequestMessage.h"
#include "ShutdownException.h"

#include <map>
#include <memory>

#include <wiicpp.h>

// class managing connections with wii devices
class DeviceManager
{
public:
    /// constructor setting members, loading config files,
    /// connecting with devices in discovery mode on program start
    DeviceManager();
    /// static function to check if a command can be handled by the DeviceManager class
    /// \param command command to be checked
    /// \return true if the command can be handled, else false
    static bool isGeneralCommand(const Command command);

    // processes events and checks if program has to be shut down
    void update();
    /// function for general, non-device-specific requests
    /// \param command command enum value of the request
    /// \param parameterString parameter of the command
    /// \return response string
    std::string handleRequestGeneral(const Command command, const std::string &parameterString);

    bool getQuitting() const;
    Device & identifierToDevice(const std::string & identifier);

private:
    // checked in update to shut down program if true
    bool quitting;
    // pointer to main CWii library object,
    // has to me allocated on heap to avoid weird connection errors
    std::unique_ptr<CWii> cWiiPtr;

    // alias to MAC address map
    std::map<std::string, std::string> aliasMacAddressMap;
    // MAC address to device pointer map
    std::map<std::string, std::unique_ptr<Device>> macAddressDevicePtrMap;

    // devicetype.cfg file location
    const std::string  deviceTypeConfigFilename = "config/devicetype.cfg";
    // alias.cfg file location
    const std::string  aliasConfigFilename = "config/alias.cfg";

    /// bitwise combined command for isGeneralCommand
    static const unsigned generalCommand = LIST | REFRESH | SHUTDOWN;

    /// uses std::regex to check if a string is a valid MAC address
    /// <a href="http://stackoverflow.com/questions/4260467/what-is-a-regular-expression-for-a-mac-address/4260512#4260512">Stack Overflow</a>
    /// \param macAddress MAC address string to be tested
    /// \return true if valid, else false
    bool validMacAddress(const std::string& macAddress) const;
    /// assigns aliasMacAddressMap based on file content
    /// \param filename alias.cfg file location
    /// \return true if loading successful, else false
    bool loadAliasConfig(const std::string & filename);
    /// assigns macAddressDevicePtrMap based on file content
    /// \param filename devicetype.cfg file location
    /// \return true if loading successful, else false
    bool loadDeviceTypeConfig(const std::string & filename);
    /// tries to add a pair into macAddressDevicePtrMap
    /// \param macAddress MAC address of device
    /// \param devicePtr pointer to device
    /// \return true if successful, else false
    bool addMacAddressDevicePair(const std::string &macAddress, std::unique_ptr<Device> devicePtr);
    /// tries to add a pair into aliasMacAddressMap
    /// \param macAddress MAC address of device
    /// \param newAlias alias to use in addition to macAddress
    /// \return true if successful, else false
    bool addAlias(const std::string & macAddress, const std::string & newAlias);
    /// processes events of devices contained in macAddressDevicePtrMap
    void processEvents();
    /// response to a command request
    /// \return string containing devices in macAddressDevicePtrMap, their MAC addresses and types
    std::string listCommandResponse() const;
    /// sets quitting member to true
    void startShutdown();

    /// synchronizes the device objects with their respective CWii library pointers
    /// \param timeout duration in seconds for which the bluetooth pairing can take place
    void synchronizeDevices(const int timeout = 1);
    /// calls desync-function on every device in macAddressDevicePtrMap
    void desynchronizeDevices();
};
