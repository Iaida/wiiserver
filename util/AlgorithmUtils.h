#pragma once
#include <algorithm>

namespace AlgorithmUtils
{

    /// removes elements from Container c for which the UnaryPredicate p holds true
    /// and returns a boolean indicating if an element was removed
    /// <a href="https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom">Wikipedia</a>
    /// \tparam Container std type that has begin and end functions
    /// \tparam UnaryPredicate lambda function that takes an element of the container and returns a boolean value
    /// \param c the Container to remove elements from
    /// \param p the UnaryPredicate lambda function that returns true for elements which should be removed from c
    template<typename Container, typename UnaryPredicate>
    inline void removeElementsIf(Container &c, UnaryPredicate p)
    {
        c.erase(std::remove_if(c.begin(), c.end(), p), c.end());
    };
}
