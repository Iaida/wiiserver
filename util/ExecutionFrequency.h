#pragma once
#include <chrono>

/// Class for testing purposes, when updates were delayed due to cwii library errors
/// usage in function:
///     static ExecutionFrequency ef;
///     ef.update();
/// => check executionsPerSecond member during debugging
class ExecutionFrequency
{
public:
    /// default constructor
    ExecutionFrequency() :
            executionCounter(0),
            executionsPerSecond(0.0),
            startTime(std::chrono::high_resolution_clock::now())
    {
    }

    /// gets called where the number of executions per second of a certain line is to be tested
    void update()
    {
        executionCounter++;

        auto currTime = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsedDuration = currTime - startTime;
        double elapsedCount = elapsedDuration.count();

        executionsPerSecond = static_cast<double>(executionCounter) / elapsedCount;
    };

    /// saves the resulting number of executions per second
    double executionsPerSecond;

private:
    /// gets incremented in update function
    unsigned executionCounter;
    /// initialized in default constructor
    std::chrono::high_resolution_clock::time_point startTime;
};