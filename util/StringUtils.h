#pragma once

#include <vector>
#include <sstream>


namespace StringUtils
{
    /// splits a string into a vector of strings divided by a character delimiter
    /// <a href="http://stackoverflow.com/questions/10058606/c-splitting-a-string-by-a-character/10058725#10058725">Stack Overflow</a>
    /// \param input string to be split
    /// \param delimiter character at which the string should be split
    /// \return vector of strings contained between occurrences of the delimiter
    inline
    std::vector<std::string> split(const std::string& input, char delimiter)
    {
        std::vector<std::string> result;
        std::stringstream ss(input);
        std::string token;

        while(getline(ss, token, delimiter))
            result.push_back(token);

        return result;
    }

    /// splits a string into a vector of strings divided by a string delimiter
    /// based on : <a href="http://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c/14266139#14266139">Stack Overflow</a>
    /// \param input string to be split
    /// \param delimiter string at which the string should be split
    /// \return vector of strings contained between occurrences of the delimiter
    inline
    std::vector<std::string> split(const std::string& input, const std::string& delimiter)
    {
        std::vector<std::string> result;
        std::string workingCopy = input;
        size_t pos = 0;
        std::string token;
        while ((pos = workingCopy.find(delimiter)) != std::string::npos)
        {
            token = workingCopy.substr(0, pos);
            result.push_back(token);
            workingCopy.erase(0, pos + delimiter.length());
        }
        result.push_back(workingCopy);
        return result;
    }

    /// tests if a string is  a valid integer
    /// <a href="https://stackoverflow.com/questions/4654636/how-to-determine-if-a-string-is-a-number-with-c/16575025#16575025">Stack Overflow</a>
    /// \param input string to be tested
    /// \return returns true if the input string is a valid integer, else false
    inline
    bool stringIsInt(const std::string & input)
    {
        char* p;
        strtol(input.c_str(), &p, 10);
        return *p == 0;
    }
}