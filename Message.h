#pragma once

#include "enum/Command.h"

/// base class representing a message sent between client and server
class Message
{
protected:
    /// default constructor
    Message() {}
    /// constructor setting members
    /// \param command command enum at the start of the message
    /// \param identifier identifier(MAC address or alias) only required for certain commands, can be left empty
    /// \param parameter parameter(s) only required for certain commands, can be left empty
    Message(const Command command, const std::string & identifier, const std::string & parameter) :
            command(command), identifier(identifier), parameter(parameter)
    {}
    /// virtual destructor
    virtual ~Message() = default;

public:
    /// command enum
    Command command;
    /// MAC address or alias only required for certain commands, can be left empty
    std::string identifier;
    /// parameter(s) only required for certain commands, can be left empty
    std::string parameter;
};