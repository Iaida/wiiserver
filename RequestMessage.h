#pragma once

#include "enum/Command.h"
#include "Message.h"

class RequestMessage : public Message
{
public:
    /// same default constructor as Message
    RequestMessage();
    /// same constructor setting members as Message
    RequestMessage(const Command command, const std::string & identifier, const std::string & parameter);
    /// custom constructor from a string, can throw different ErrorCodeExceptions
    /// \param requestString string received from a client used for construction
    RequestMessage(const std::string &requestString);
};