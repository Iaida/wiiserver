#include "DeviceManager.h"
#include "util/StringUtils.h"
#include "WiiMote.h"
#include "BalanceBoard.h"
#include "ErrorCodeException.h"
#include "ResponseMessage.h"

#include <algorithm>
#include <regex>
#include <fstream>

DeviceManager::DeviceManager() : quitting(false)
{
    if (!loadDeviceTypeConfig(deviceTypeConfigFilename) || !loadAliasConfig(aliasConfigFilename))
        throw std::runtime_error("error loading config files");

    synchronizeDevices(3);
}


Device & DeviceManager::identifierToDevice(const std::string &identifier)
{
    // check if identifier is an alias, set macAddress variable accordingly
    auto aliasSearchResult = aliasMacAddressMap.find(identifier);
    std::string macAddress = (aliasSearchResult != aliasMacAddressMap.end()) ?
                             aliasSearchResult->second :
                             identifier;

    // check if macAddress is the key of a device
    auto macSearchResult = macAddressDevicePtrMap.find(macAddress);
    if (macSearchResult != macAddressDevicePtrMap.end() && macSearchResult->second->synchronized())
        return *(macSearchResult->second);
    else
        throw DeviceNotFound();
}

std::string DeviceManager::listCommandResponse() const
{
//        (S) LIST:(<MAC>,<id>[,alias],<devicetype>;)*
//        (S) LIST:11-22-33-44-22-11,1,cave-wiimote,dt_wm;AA-BB-CC-AA-BB-CC,2,dt_bb

    std::string result;
    for (const auto & macAddressDevicePair : macAddressDevicePtrMap)
    {
        if (macAddressDevicePair.second->synchronized())
        {
            result += macAddressDevicePair.first + ",";
            for (const auto & aliasMacAddressPair : aliasMacAddressMap)
            {
                // same mac address
                if (aliasMacAddressPair.second == macAddressDevicePair.first)
                    result += aliasMacAddressPair.first + ",";
            }
            result += deviceTypeToString(macAddressDevicePair.second->getDeviceType()) + ";";
        }
    }
    // remove trailing semicolon
    if (result.back() == ';')
        result.pop_back();
    return ResponseMessage(Command::LIST, "", result).toString();
}

void DeviceManager::processEvents()
{
    // poll and check if an event occurred on any device
    if (cWiiPtr->Poll() > 0)
    {
        for (auto & macAddressDevicePair : macAddressDevicePtrMap)
            macAddressDevicePair.second->processEvents();
    }
}


bool DeviceManager::validMacAddress(const std::string &macAddress) const
{
    static const std::regex macRegex("([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})");
    return std::regex_match(macAddress, macRegex);
}

bool DeviceManager::loadAliasConfig(const std::string &filename)
{
    aliasMacAddressMap.clear();

    // syntax:
    // <MAC> <alias>
    // 00-19-1D-C5-A3-94 wii-cave
    // 34-AF-2C-E7-60-DF balance

    std::ifstream file(filename);
    std::string line;
    while (std::getline(file, line))
    {
        std::vector<std::string> splitResult = StringUtils::split(line, ' ');
        std::string macAddress = splitResult[0];
        if (splitResult.size() >= 2 && validMacAddress(macAddress))
        {
            for (size_t i = 1; i < splitResult.size(); i++)
                addAlias(macAddress, splitResult[i]);
        }
        else
            return false;
    }
    return true;
}

bool DeviceManager::loadDeviceTypeConfig(const std::string &filename)
{
    macAddressDevicePtrMap.clear();

    // syntax:
    // <MAC> <type>
    // 34-AF-2C-E7-60-DF dt_bb

    std::ifstream file(filename);
    std::string line;
    while (std::getline(file, line))
    {
        std::vector<std::string> splitResult = StringUtils::split(line, ' ');
        if (splitResult.size() == 2 && validMacAddress(splitResult[0]))
        {
            try
            {
                DeviceType deviceType = stringToDeviceType(splitResult[1]);

                // add device
                switch (deviceType)
                {
                    case WIIMOTE:
                        addMacAddressDevicePair(splitResult[0], std::make_unique<WiiMote>());
                        break;
                    case BALANCEBOARD:
                        addMacAddressDevicePair(splitResult[0], std::make_unique<BalanceBoard>());
                        break;
                }
            }
            catch (const DeviceNotFound &e)
            {
                return false;
            }
        }
        else
            return false;
    }
    return true;
}

bool DeviceManager::addMacAddressDevicePair(const std::string &macAddress, std::unique_ptr<Device> devicePtr)
{
    return macAddressDevicePtrMap.emplace(macAddress, std::move(devicePtr)).second;
}

bool DeviceManager::addAlias(const std::string &macAddress, const std::string &newAlias)
{
    return aliasMacAddressMap.emplace(newAlias, macAddress).second;
}


void DeviceManager::synchronizeDevices(const int timeout)
{
    // reset relevant member variables
    desynchronizeDevices();
    cWiiPtr = std::make_unique<CWii>(static_cast<int>(macAddressDevicePtrMap.size()));

    std::cout << "synchronizeDevices: looking for devices in discovery mode... \n";
    std::cout << "FIND, CONNECT MESSAGES:\n";
    // parameters: timeout, rumble ack, autoreconnect
    cWiiPtr->FindAndConnect(timeout, false, false);

    std::cout << "synchronizeDevices: number of connected devices: " << cWiiPtr->GetNumConnectedWiimotes() << "\n";

    // default true parameter of GetWiimotes includes RefreshWiimotes() to handle disconnects
    for (CWiimote & wm : cWiiPtr->GetWiimotes(true))
    {
        std::string address(wm.GetAddress());
        // replace ':' with '-' to fit own config and map syntax
        std::replace(address.begin(), address.end(), ':', '-');

        // check if address-string is the key of a device
        auto macSearchResult = macAddressDevicePtrMap.find(address);
        if (macSearchResult != macAddressDevicePtrMap.end())
        {
            // assign connected CWiimote to device in map
            macSearchResult->second->setCWiimotePtr(&wm);
            std::cout << "synchronizeDevices: device with address " + address + " connected \n";
        }
        else
            std::cout << "synchronizeDevices: device with address " + address + " not included in config file \n";
    }
};

std::string DeviceManager::handleRequestGeneral(const Command command, const std::string &parameterString)
{
    switch (command)
    {
        case LIST:
            // LIST
            if (parameterString.empty())
                return listCommandResponse();
            else
                throw NotAValidParameter();
        case REFRESH:
            // REFRESH
            if (parameterString.empty())
            {
                synchronizeDevices();
                return "REFRESH:done\n";
            }
            else if (StringUtils::stringIsInt(parameterString))
            {
                synchronizeDevices(std::stoi(parameterString));
                return "REFRESH:done\n";
            }
            else
                throw NotAValidParameter();
        case SHUTDOWN:
            // SHUTDOWN
            // shutdown server
            if (parameterString.empty())
                startShutdown();
            else
                throw NotAValidParameter();
            return "";
        default:
            throw NotAValidCommand();
    }
}

bool DeviceManager::isGeneralCommand(const Command command)
{
    return command & generalCommand;
}

void DeviceManager::desynchronizeDevices()
{
    for (auto & macAddressDevicePtrPair : macAddressDevicePtrMap)
        macAddressDevicePtrPair.second->desync();
}

void DeviceManager::update()
{
    if (quitting)
        throw ShutdownException("shutdown");
    processEvents();
}

void DeviceManager::startShutdown()
{
    std::cout << "starting shutdown... \n";
    quitting = true;
}

bool DeviceManager::getQuitting() const
{
    return quitting;
}
