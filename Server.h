#pragma once

#include <boost/asio.hpp>
#include "Session.h"
#include "DeviceManager.h"

/// class managing new incoming connections and client sessions
class server
{
public:
    /// constructor setting members and starting the accept-process for incoming connections
    /// \param io_service reference to the boost io_service object
    /// \param deviceManager reference to the device manager to communicate with main thread
    server(boost::asio::io_service & io_service, DeviceManager & deviceManager)
            : acceptor_(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
              socket_(io_service),
              deviceManager(deviceManager)
    {
        do_accept();
    }

private:
    /// recursing function accepting new connections and managing client sessions with shared pointers
    void do_accept()
    {
        acceptor_.async_accept(socket_,
                               [this](boost::system::error_code ec)
                               {
                                   if (!ec)
                                   {
                                       std::make_shared<session>(std::move(socket_), deviceManager)->start();
                                   }

                                   do_accept();
                               });
    }

    /// boost acceptor needed to accept new connections
    boost::asio::ip::tcp::acceptor acceptor_;
    /// boost socket
    boost::asio::ip::tcp::socket socket_;
    /// default port
    static const short port = 1300;
    /// reference to the device manager to communicate with main thread
    DeviceManager & deviceManager;
};