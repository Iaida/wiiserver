#include "Session.h"
#include "util/Timer.h"
#include "util/AlgorithmUtils.h"
#include "util/StringUtils.h"
#include <boost/date_time.hpp>

session::session(boost::asio::ip::tcp::socket socket, DeviceManager &deviceManager)
        : socket_(std::move(socket)),
          deviceManager(deviceManager),
          streaming(false)
{
    ipString = socket_.remote_endpoint().address().to_string();
}


session::~session()
{
    if (readStreaming())
        stopStreaming();
    if (streamThread.joinable())
        streamThread.join();
}

void session::stopStreaming()
{
    setStreaming(false);
}

bool session::readStreaming()
{
    std::lock_guard<std::mutex> lock(streamingMutex);
    return streaming;
}

void session::setStreaming(bool streamRunning)
{
    std::lock_guard<std::mutex> lock(streamingMutex);
    streaming = streamRunning;
}

void session::startStreaming()
{
    setStreaming(true);
    // necessary before destroying old thread member and replacing it
    if (streamThread.joinable())
        streamThread.join();

    streamThread = std::thread(
            [&]()
            {
                Timer timer(Timer::hz100);
                while (true)
                {
                    if (timer.check())
                    {
                        try
                        {
                            if (readStreaming())
                            {
                                for (auto &IdentifierDevicePtrPair : identifierDeviceStreams)
                                {
                                    std::string messageToWrite = IdentifierDevicePtrPair.second->streamMessage(IdentifierDevicePtrPair.first);
                                    do_stream_write(messageToWrite);
                                }
                            }
                            else
                            {
                                // terminate thread when no longer streaming
                                return;
                            }
                        }
                        catch (std::exception& e)
                        {
                            std::cerr << "startStreaming Exception: " << e.what() << "\n";
//                            stopStreaming();
                            return;
                        }
                    }
                    timer.sleepRestDuration();
                }
            }
    );
}


void session::do_stream_write(const std::string message)
{
    auto self(shared_from_this());
    if (readStreaming())
    {
        boost::system::error_code ec;
        boost::asio::write(socket_, boost::asio::buffer(message), ec);
        if (!ec)
        {
            //std::cout << debugPrefix() << "received stream message \n";
        }
        else if (ec == boost::asio::error::eof)
        {
            stopStreaming();
            std::cout << debugPrefix() + "do_stream_write disconnected: eof \n";

        }
        else if (ec == boost::asio::error::connection_reset)
        {
            stopStreaming();
            std::cout << debugPrefix() + "do_stream_write disconnected: connection_reset \n";

        }
        else if (ec == boost::asio::error::broken_pipe)
        {
            stopStreaming();
            std::cout << debugPrefix() + "do_read broken_pipe \n";

        }
        else
        {
            std::cout << debugPrefix() + "do_stream_write error: " + ec.message() + "\n";
            throw std::runtime_error(ec.message());
            //stopStreaming();
        }
    }
}

void session::do_write(const std::string &message)
{
    auto self(shared_from_this());
    boost::asio::async_write(socket_, boost::asio::buffer(message),
                             [this, self](boost::system::error_code ec, std::size_t /*length*/)
                             {
                                 if (!ec)
                                 {
                                     //std::cout << debugPrefix() << "wrote message successfully \n";
                                 }
                                 else if (ec == boost::asio::error::eof)
                                 {
                                     stopStreaming();
                                     std::cout << debugPrefix() + "do_write disconnected: eof \n";

                                 }
                                 else if (ec == boost::asio::error::connection_reset)
                                 {
                                     stopStreaming();
                                     std::cout << debugPrefix() + "do_write disconnected: connection_reset \n";

                                 }
                                 else if (ec == boost::asio::error::broken_pipe)
                                 {
                                     stopStreaming();
                                     std::cout << debugPrefix() + "do_read broken_pipe \n";

                                 }
                                 else
                                 {
                                     stopStreaming();
                                     std::cout << debugPrefix() + "do_write error: " + ec.message() + "\n";

                                 }
                             });
}

void session::do_read()
{
    auto self(shared_from_this());

    socket_.async_read_some(boost::asio::buffer(charBuffer),
                            [this, self](boost::system::error_code ec, std::size_t length)
                            {
                                if (!ec)
                                {
                                    std::string rawDataString = std::string(charBuffer.data());
                                    // clear data for next read, to avoid leftovers from old longer messages
                                    charBuffer.fill(0);

//                                    std::cout << debugPrefix() + "rawDataString: " + rawDataString + "\n";

                                    AlgorithmUtils::removeElementsIf(rawDataString, [](const char c){return c == '\r';});
                                    std::vector<std::string> filteredRequests = StringUtils::split(rawDataString, '\n');

                                    bool quitRequestOccurred = false;

                                    for (auto & filteredRequest : filteredRequests)
                                    {
                                        std::cout << debugPrefix() + "request message: " + filteredRequest + "\n";


                                        RequestMessage requestMessage;
                                        std::string response;
                                        try
                                        {
                                            // parse string to message
                                            requestMessage = RequestMessage(filteredRequest);

                                            if (isSessionCommand(requestMessage.command))
                                                response = handleSessionRequest(requestMessage.command, requestMessage.identifier, requestMessage.parameter);
                                            else if (DeviceManager::isGeneralCommand(requestMessage.command))
                                                response = deviceManager.handleRequestGeneral(requestMessage.command, requestMessage.parameter);
                                            else
                                            {
                                                Device & device = deviceManager.identifierToDevice(requestMessage.identifier);
                                                response = device.handleRequest(requestMessage.command, requestMessage.identifier, requestMessage.parameter);
                                            }
                                        }
                                        catch (const ErrorCodeException & e)
                                        {
                                            std::cout << "ErrorCode: " << e.errorCode << ", message: " << e.what() << "\n";
                                            response = "ERROR:" + std::to_string(e.errorCode) + "\n";
                                        }

                                        if (!response.empty())
                                        {
                                            std::cout << debugPrefix() + "response message: " + response;
                                            do_write(response);
                                        }

                                        if (requestMessage.command == QUIT)
                                            quitRequestOccurred = true;

                                    }
                                    // recursion ends when quitting
                                    if (!quitRequestOccurred && !deviceManager.getQuitting())
                                        do_read();
                                }
                                else if (ec == boost::asio::error::eof)
                                {
                                    stopStreaming();
                                    std::cout << debugPrefix() + "do_read disconnected: eof \n";
                                }
                                else if (ec == boost::asio::error::connection_reset)
                                {
                                    stopStreaming();
                                    std::cout << debugPrefix() + "do_read disconnected: connection_reset \n";
                                }
                                else if (ec == boost::asio::error::broken_pipe)
                                {
                                    stopStreaming();
                                    std::cout << debugPrefix() + "do_read broken_pipe \n";
                                }
                                else
                                {
                                    stopStreaming();
                                    std::cout << debugPrefix() + "do_read error: " + ec.message() + "\n";
                                }
                            });
}

void session::start()
{
    do_read();
    std::cout << debugPrefix() +  "session started \n";
}

bool session::isSessionCommand(const Command command)
{
    return command & sessionCommand;
}

std::string session::handleSessionRequest(const Command command, const std::string &identifier,
                                          const std::string &parameter)
{
    switch (command)
    {
        case STREAM:
            if (parameter.empty())
            {
                identifierDeviceStreams.emplace_back(identifier, &deviceManager.identifierToDevice(identifier));
                std::cout << debugPrefix() + "added device belonging to identifier <" + identifier + "> to identifierDeviceStreams \n";
                // start streaming if not already running
                if (!readStreaming())
                    startStreaming();
            }
            else if (parameter == "stop")
            {
                std::cout << debugPrefix() + "remove device belonging to " + identifier +  " from identifierDeviceStreams \n";
                AlgorithmUtils::removeElementsIf(
                        identifierDeviceStreams,
                        [&](const std::pair<std::string, Device*> identifierDevicePtrPair)
                        {
                            return &deviceManager.identifierToDevice(identifier) == identifierDevicePtrPair.second;
                        });

                // stop streaming if running and no devices to stream from in vector
                if (readStreaming() && identifierDeviceStreams.empty())
                    stopStreaming();
            }
            else
                throw NotAValidParameter();
            break;
        case QUIT:
            if (parameter.empty())
            {
                std::cout << debugPrefix() + "quit command, closing connection... \n";
                stopStreaming();
                socket_.close();
            }
            else
                throw NotAValidParameter();
            break;
        default:
            throw NotAValidCommand();
    }

    return "";
}


std::string session::debugPrefix() const
{

    return '[' + getTimeStamp() + "] [" + ipString + "]: ";
}

std::string session::getTimeStamp() const
{

    const boost::posix_time::time_duration now = boost::posix_time::second_clock::local_time().time_of_day();
    // return up to decimals of seconds
    return boost::posix_time::to_simple_string(now).substr(0, 8);

}

