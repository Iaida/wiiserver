#pragma once

#include <stdexcept>

/// base class for a custom exception with an associated error code
class ErrorCodeException : public std::logic_error
{
public:
    /// constructor with a message and error code
    /// \param message message to match the errorCode parameter
    /// \param errorCode error code which will be received by the client
    ErrorCodeException(const std::string & message, const unsigned errorCode) :
            std::logic_error(message),
            errorCode(errorCode)
    {}

    const unsigned errorCode;
};

/// custom exception for an unvalid command
class NotAValidCommand : public ErrorCodeException
{
public:
    /// default constructor setting members
    NotAValidCommand() : ErrorCodeException("not a valid command", 101)
    {}

};

/// custom exception for an unvalid parameter
class NotAValidParameter : public ErrorCodeException
{
public:
    /// default constructor setting members
    NotAValidParameter() : ErrorCodeException("not a valid parameter", 102)
    {}

};

/// custom exception for device which could not be found due to an invalid identifier
class DeviceNotFound : public ErrorCodeException
{
public:
    /// default constructor setting members
    DeviceNotFound() : ErrorCodeException("device not found", 201)
    {}

};

/// custom exception for incompatible command and device
class CommandAndDeviceNotCompatible : public ErrorCodeException
{
public:
    /// default constructor setting members
    CommandAndDeviceNotCompatible() : ErrorCodeException("command and device not compatible", 202)
    {}

};
