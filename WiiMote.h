#pragma once

#include "Device.h"
#include "enum/LED.h"

/// class representing a wiimote which handles events, requests and
/// uses cwii library-functionality to access the current wiimote state
class WiiMote : public Device
{
public:
    WiiMote();
    /// function used to handle wiimote-specific events
    void processEvents() override;
    /// setter, which synchronizes the device object with the cwii-library's object and sets wiimote-specific options
    /// \param cWiimotePtr the CWiimote pointer to be used
    void setCWiimotePtr(CWiimote *cWiimotePtr) override;
    /// function for handling wiimote-specific requests
    /// \param command command enum value of the request
    /// \param identifierString MAC address or alias of the device which should handle the request
    /// \param parameterString parameter of the command
    /// \return response string
    std::string handleRequest(const Command command, const std::string & identifierString, const std::string & parameterString) override;
    /// static function to check if a command can be handled by the WiiMote class
    /// \param command command to be checked
    /// \return true if the command can be handled, else false
    static bool isWiiMoteCommand(const Command command);
    /// function for sending messages when streaming the wiimote state
    /// \param identifier MAC address or alias of the device which should handle the request
    /// \return response string containing the device state
    std::string streamMessage(const std::string &identifier) override;
    DeviceType getDeviceType() const override;

private:
    /// bitwise combined command for isWiiMoteCommand
    static const unsigned wiiMoteCommand = TYPE | BATTERY | GET  | ROT | RUMBLE | SET;

    /// returns cwii library led enum bitmask matching an index from 1 to 4
    /// \param index values 1 to 4
    /// \return cwii library led enum bitmask matching index
    CWiimote::LEDS ledIndexToBitmask(const int index) const;
    /// sets an LED to on or off
    /// \param led LED value from custom LED enum
    /// \param on true for on, false for off
    void setLEDEnum(const LED led, const bool on);
    /// returns orientation string used in responses with values set to zero if no accelerometer detected
    /// \return string in the form '<roll>,<pitch>,<yaw>'
    std::string getOrientationString();
    /// returns current led state
    /// \param led LED value from custom LED enum
    /// \return returns true for on, false for off
    bool getLEDState(const LED led);
};