#pragma once

#include <string>
#include <map>
#include "../ErrorCodeException.h"


/// Command enum with values bit-shifted to check for the type of command
/// <a href="https://stackoverflow.com/questions/15181579/c-most-efficient-way-to-compare-a-variable-to-multiple-values/15181885#15181885">Stack Overflow</a>
enum Command
{
    ERROR = 1 << 0,
    LIST = 1 << 1,
    TYPE = 1 << 2,
    REFRESH = 1 << 3,
    QUIT = 1 << 4,
    SHUTDOWN = 1 << 5,
    GET = 1 << 6,
    STREAM = 1 << 7,
    ROT = 1 << 8,
    RUMBLE = 1 << 9,
    SET = 1 << 10,
    BATTERY = 1 << 11
};

/// map to match strings to Command enum
const std::map<std::string, Command> stringCommandMap
        {
                {"ERROR", ERROR},
                {"LIST", LIST},
                {"TYPE", TYPE},
                {"REFRESH", REFRESH},
                {"QUIT", QUIT},
                {"SHUTDOWN", SHUTDOWN},
                {"GET", GET},
                {"STREAM", STREAM},
                {"ROT", ROT},
                {"RUMBLE", RUMBLE},
                {"SET", SET},
                {"BATTERY", BATTERY}
        };

/// returns a Command enum value or throws NotAValidCommand exception if no match found
/// \param string input string which should be occurring in stringCommandMap
/// \return matching Command enum value
inline
Command stringToCommand(const std::string & string)
{
    auto searchResult = stringCommandMap.find(string);
    if (searchResult != stringCommandMap.end())
        return searchResult->second;
    else
        throw NotAValidCommand();
}

/// returns a matching string or throws runtime exception if no match found
/// \param command input command which should be occurring in stringCommandMap
/// \return matching string
inline
std::string commandToString(const Command command)
{
    for (auto & stringCommandPair : stringCommandMap)
    {
        if (stringCommandPair.second == command)
            return stringCommandPair.first;
    }
    throw std::runtime_error("stringCommandMap is not set up properly");
}
