#pragma once

#include <string>
#include <map>
#include "../ErrorCodeException.h"
#include <wiicpp.h>

/// map to match strings to the cwii library's button enum
const std::map<std::string, CButtons::ButtonDefs> stringButtonMap =
        {
                {"btn_a", CButtons::BUTTON_A},
                {"btn_b", CButtons::BUTTON_B},
                {"btn_one", CButtons::BUTTON_ONE},
                {"btn_two", CButtons::BUTTON_TWO},
                {"btn_home", CButtons::BUTTON_HOME},
                {"btn_left", CButtons::BUTTON_LEFT},
                {"btn_right", CButtons::BUTTON_RIGHT},
                {"btn_down", CButtons::BUTTON_DOWN},
                {"btn_up", CButtons::BUTTON_UP},
                {"btn_minus", CButtons::BUTTON_MINUS},
                {"btn_plus", CButtons::BUTTON_PLUS}
        };

/// returns a cwii button enum value or throws NotAValidParameter exception if no match found
/// \param string input string which should be occurring in stringButtonMap
/// \return matching cwii button enum value
inline
CButtons::ButtonDefs stringToButton(const std::string & string)
{
    auto searchResult = stringButtonMap.find(string);
    if (searchResult != stringButtonMap.end())
        return searchResult->second;
    else
        throw NotAValidParameter();
};
