#pragma once

#include <string>
#include <map>

#include "../ErrorCodeException.h"
#include <wiicpp.h>


/// own LED enum where the leds are matched with their index for the 'isLEDSet'-function of the cwii library
enum LED
{
    LED_ALL = 0,
    LED_1 = 1,
    LED_2 = 2,
    LED_3 = 3,
    LED_4 = 4
};


/// map to match strings to LED enum
const std::map<std::string, LED> stringLEDMap =
        {
                {"led_all", LED_ALL},
                {"led_1", LED_1},
                {"led_2", LED_2},
                {"led_3", LED_3},
                {"led_4", LED_4}
        };

/// returns a LED enum value or throws NotAValidParameter exception if no match found
/// \param string input string which should be occurring in stringLEDMap
/// \return matching LED enum value
inline
LED stringToLED(const std::string & string)
{
    auto searchResult = stringLEDMap.find(string);
    if (searchResult != stringLEDMap.end())
        return searchResult->second;
    else
        throw NotAValidParameter();
};