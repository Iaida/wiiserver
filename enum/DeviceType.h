#pragma once

#include <string>
#include <map>
#include "../ErrorCodeException.h"

/// enum which could be extended for additional wii devices
enum DeviceType
{
    WIIMOTE,
    BALANCEBOARD,
};


/// map to match strings to DeviceType enum
const std::map<std::string, DeviceType> stringDeviceTypeMap
        {
                {"dt_wm", WIIMOTE},
                {"dt_bb", BALANCEBOARD}
        };

/// returns a DeviceType enum value or throws DeviceNotFound exception if no match found
/// \param string input string which should be occurring in stringDeviceTypeMap
/// \return matching DeviceType enum value
inline
DeviceType stringToDeviceType(const std::string & string)
{
    auto searchResult = stringDeviceTypeMap.find(string);
    if (searchResult != stringDeviceTypeMap.end())
        return searchResult->second;
    else
        throw DeviceNotFound();
}

/// returns a matching string or throws runtime exception if no match found
/// \param deviceType input deviceType which should be occurring in stringDeviceTypeMap
/// \return matching string
inline
std::string deviceTypeToString(const DeviceType deviceType)
{
    for (auto & stringDeviceTypePair : stringDeviceTypeMap)
    {
        if (stringDeviceTypePair.second == deviceType)
            return stringDeviceTypePair.first;
    }
    throw std::runtime_error("stringDeviceTypeMap is not set up properly");
}