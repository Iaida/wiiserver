# Errorcodes

Error Response Syntax: ERROR:\<ErrorCode>

| Errorcode | Description |
| ------ | ------ |
| 101 | not a valid command | 
| 102 | commando ok, not a valid parameter |
| 201 | device/identifier not found |
| 202 | command and device/identifier not compatible | 
