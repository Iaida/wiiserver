# Wiimote Requests and Responses

### Parameters

| Parameter | Valid Parameters |
| ------ | ------ |
| \<btn> | btn_a \| btn_b \| btn_one \| btn_two \| btn_home \| btn_up \| btn_right \| btn_down \| btn_left \| btn_minus \| btn_plus |
| \<led> | led_1 \| led_2 \| led_3 \| led_4 \| led_all |


### GET Command

get current state of buttons 

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax| GET \<identifier> \<btn> |
| Response Syntax| GET:\<identifier>;up\|down |
| Request Example| GET wm btn_right |
| Response Example|  GET:wm;up |

get current state of leds 

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax| GET \<identifier> \<led> |
| Response Syntax| GET:\<identifier>;off\|on |
| Request Example| GET wm led_1 |
| Response Example|  GET:wm;off |

### STREAM Command

starts/stops stream sending rotation and button states at a rate of 100Hz

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | STREAM \<identifier> [stop] |
| Response Syntax | STREAM:\<identifier>;ROT:\<roll>,\<pitch>,\<yaw>;BTN:\<btn>,\<state>;BTN:\<btn>,\<state>;... |
| Request Example 1 | STREAM wm |
| Response Example 1 | STREAM:wm;ROT:-55.302475,-19.843306,0.000000; BTN:btn_a,up;BTN:btn_b,up;BTN:btn_down,up;BTN:btn_home,up;BTN:btn_left,up;BTN:btn_minus,up; BTN:btn_one,up;BTN:btn_plus,up;BTN:btn_right,up;BTN:btn_two,up;BTN:btn_up,up|
| Request Example 2 | STREAM wm stop |
| Response Example 2 | none, except for errors |

### ROT Command

get current orientation of wiimote, yaw always 0.0

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax| ROT \<identifier> |
| Response Syntax| ROT:\<identifier>;\<roll>,\<pitch>,\<yaw> |
| Request Example| ROT wm |
| Response Example|  ROT:wm;30.1023,19.393,0.000 |

### RUMBLE Command

activates/deactivates the wiimote's rumble

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax| RUMBLE \<identifier> on\|off |
| Response Syntax| none, except for errors |
| Request Example| RUMBLE wm on |

### SET Command for LEDs

turns the wiimote's led lights on or off

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax| SET \<identifier> \<led> on\|off |
| Response Syntax| none, except for errors |
| Request Example| SET wm led_1 on |

