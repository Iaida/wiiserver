# Readme

### Wiki Content

* [Requests and Responses](requests-and-responses.md) 
	* [Wiimote Requests and Responses](wiimote-requests-and-responses.md) 
	* [Balanceboard Requests and Responses](balanceboard-requests-and-responses.md) 
* [Errorcodes](errorcodes.md) 




### Libraries

* wiic  library (http://wiic.sourceforge.net/)
    * Dependencies: 
        * gcc
        * g++
        * cmake
        * libbluetooth-dev
* Boost library

### Network Communation

* Port: 1300
* Testing with netcat
    * nc \<IP> 1300

### Connecting to Wii Devices  

| Device Type | Discovery Button(s) | Discovery Mode Indicator |
| ------ | ------ | ------ | 
| Wiimote | Pressing 1 and 2 at the same time | all four LEDs will flash |
| Balanceboard | Pressing button near batteries | LED on side will flash |

* Program start: Searches and connects with devices in discovery mode near bluetooth dongle.

* REFRESH-command: Disconnects from connected devices, then searches and connects with devices in discovery mode near bluetooth dongle while program is already running.

* Connecting both Wiimote and Balanceboard at the same time during refresh: When connecting both the Wiimote and Balanceboard at the same time during refresh, the user has to put the balance board in discovery mode first before the wiimote, else the program will not detect the expansion-device of the balance board and won't receive any weight-values.
  This can be avoided by putting the devices in discovery-mode before sending the refresh-request.

### Wiki Syntax

| Syntax | Description |
| ------ | ------ |
|  \<x> | x has to match certain restrictions, which are tested and can result in errorcode responses | 
|  \[ x \] | x is optional, can be left out | 
| x\|y | either x or y | 

### Configuration Files

files should be present in a folder named 'config' in the same folder as the executable

| Filename | Description | Syntax
| ------ | ------ |  ------ |
|  alias.cfg | devices can be identified with multiple aliases in addition to their MAC address | \<mac> alias1 alias2 ... |
|  devicetype.cfg | matches the MAC addresses to the type of devices to determine beforehand which type of object has to be instantiated for each identifier | \<mac> dt_bb\|dt_wm |

### Rumble, LEDs and Disconnecting

When wiimote rumble or LEDs are turned on by a client and they will not be automatically turned off when disconnecting.  Rumble and LEDs will be turned off automatically only when the server shuts down.

### Remaining errors

 `cWiimotePtr->isLEDSet(4)` is always false, even if the fourth LED is on, which means that 'GET wm led_4' as well as 'GET wm led_all' always respond with 'GET:wm;off'

### TODO

Nunchuk Support
