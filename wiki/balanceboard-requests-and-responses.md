# Balanceboard Requests and Responses

### GET Command

get current weight distribution once

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | GET \<identifier> |
| Response Syntax | GET:\<identifier>;\<top-left>,\<top-right>,\<bottom-left>,\<bottom-right>,\<total> |
| Request Example | GET bb |
| Response Example | GET:bb;23.234,12.123,53.111,64.212,152.68 |

### STREAM Command

starts/stops stream sending weight distribution at a rate of 100Hz

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | STREAM \<identifier> [stop] |
| Response Syntax | STREAM:\<identifier>;\<top-left>,\<top-right>,\<bottom-left>,\<bottom-right>,\<total> |
| Request Example 1 | STREAM bb |
| Response Example 1 | STREAM:bb;23.234,12.123,53.111,64.212,152.68 |
| Request Example 2 | STREAM bb stop |
| Response Example 2 | none, except for errors |