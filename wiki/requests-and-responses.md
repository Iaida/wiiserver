# Requests and Responses

### Syntax

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | \<command> \<identifier> \<parameter> |
| Response Syntax | \<command>:[\<identifier>;]\<parameter> |

# General Commands:

### LIST Command

lists all synchronized devices with their MAC address, aliases and type

| Message Type | Message Content |
| ------ | ------ |
| Request | LIST |
| Response Syntax | \<MAC>,[alias1,][alias2,][...]\<devicetype>[;...] |
| Response Example | LIST:11-22-33-44-22-11,1,wm,dt_wm;AA-BB-CC-AA-BB-CC,2,bb,dt_bb |

### REFRESH Command

Disconnects from connected devices, then searches and connects with devices in discovery mode

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | REFRESH [\<seconds>] |
| Request Example 1 | REFRESH  |
| Response Example 1 after 1 second | REFRESH:done |
| Request Example 1 | REFRESH 5  |
| Response Example 1 after 5 seconds | REFRESH:done |

### QUIT Command

Disconnects the client

| Message Type | Message Content |
| ------ | ------ |
| Request | QUIT |
| Response | none, except for errors |

### SHUTDOWN Command

Shuts down the program and any streams still running

| Message Type | Message Content |
| ------ | ------ |
| Request | SHUTDOWN |
| Response | none, except for errors |


# Commands for all devices:

### TYPE Command

responds with type corresponding to the device matching \<identifier>

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | TYPE \<identifier> |
| Response Syntax | TYPE:\<identifier>;dt_bb\|dt_wm |
| Request Example | TYPE wm |
| Response Example | TYPE:wm;dt_wm |

### BATTERY Command

responds with the current battery level corresponding to the device matching \<identifier>

| Message Type | Message Content |
| ------ | ------ |
| Request Syntax | BATTERY \<identifier> |
| Response Syntax | BATTERY:\<identifier>;\<0.0-1.0> |
| Request Example | BATTERY bb |
| Response Example | BATTERY:bb;0.95 |
